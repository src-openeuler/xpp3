Name:           xpp3
Version:        1.1.4
Release:        22
License:        ASL 1.1
Summary:        Xml Pull Parser 3rd Edition
URL:            http://www.extreme.indiana.edu/xgws/xsoap/xpp/mxp1/index.html
Source0:        http://www.extreme.indiana.edu/dist/java-repository/xpp3/distributions/xpp3-1.1.4c_src.tgz
Source1:        https://repo1.maven.org/maven2/xpp3/xpp3/1.1.4c/xpp3-1.1.4c.pom
Source2:        https://repo1.maven.org/maven2/xpp3/xpp3_xpath/1.1.4c/xpp3_xpath-1.1.4c.pom
Source3:        https://repo1.maven.org/maven2/xpp3/xpp3_min/1.1.4c/xpp3_min-1.1.4c.pom
Source4:        xpp3-1.1.4c-OSGI-MANIFEST.MF
Provides:       xpp3-minimal = %{version}-%{release}
Obsoletes:      xpp3-minimal < %{version}-%{release}
BuildRequires:  javapackages-local java-javadoc ant junit
BuildArch:      noarch

%description
MXP1 is a stable XmlPull parsing engine that is based on ideas from XPP and in particular XPP2
but completely revised and rewritten to take the best advantage of latest JIT JVMs such as Hotspot
in JDK 1.4+.

%package        help
Summary:        Documentation for xpp3
Requires:       java-javadoc
Provides:       xpp3-javadoc = %{version}-%{release}
Obsoletes:      xpp3-javadoc < %{version}-%{release}

%description    help
Documentation for xpp3.

%prep
%autosetup -n xpp3-1.1.4c
rm -rf *.jar src/java/builder/javax
sed -i 's|depends="junit_main,junit_addons"|depends="junit_main"|' build.xml
sed -i '/<javadoc/aadditionalparam="-Xdoclint:none"' build.xml
%build
export CLASSPATH=$(build-classpath junit)
ant xpp3 junit apidoc
jar ufm build/xpp3-1.1.4c.jar %{SOURCE4}
%install
ret=0
%if 0%(find %{_buildrootdir} -name xpp3.xml)
for line in `grep -n uuid $(find %{_buildrootdir} -name xpp3.xml) | cut -d \: -f 1`
do
    sed -i "${line}s/[0-9a-f]\{8\}-[0-9a-f]\{4\}-[0-9a-f]\{4\}-[0-9a-f]\{4\}-[0-9a-f]\{12\}/f2f42549-cfab-4d71-be48-5e9f9a41e5f${ret}/g" $(find %{_buildrootdir} -name xpp3.xml)
    ret=`expr $ret + 1`
done
%endif
%mvn_file ':{*}' @1
%mvn_package :xpp3_min minimal
%mvn_artifact %{SOURCE1} build/xpp3-1.1.4c.jar
%mvn_artifact %{SOURCE2} build/xpp3_xpath-1.1.4c.jar
%mvn_artifact %{SOURCE3} build/xpp3_min-1.1.4c.jar
%mvn_install -J doc/api

%files -f .mfiles
%files -f .mfiles-minimal
%doc README.html doc/*.txt doc/*.html LICENSE.txt

%files help -f .mfiles-javadoc

%changelog
* Mon Mar 13 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 1.1.4-22
- Fixed the blockage of using the 'grep -n' command when xpp3 .xml does not exist

* Tue Dec 3 2019 Ling Yang <lingyang2@huawei.com> - 1.1.4-20
- Package init
